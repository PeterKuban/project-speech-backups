/*
SUBTITLES LIKE THIS:

00:00:00.000 --> 00:00:09.900 align:start position:19%
it<00:00:00.089><c> is</c><00:00:00.480><c> truly</c><00:00:00.810><c> great</c><00:00:01.140><c> to</c><00:00:02.340><c> be</c><00:00:02.490><c> back</c><00:00:02.520><c> here</c><00:00:03.120><c> in</c>

00:00:03.360 --> 00:00:12.509 align:start position:19%
<c.colorE5E5E5>Michigan<00:00:04.470><c> right</c><00:00:06.029><c> and</c><00:00:08.970><c> it's</c><00:00:09.179><c> also</c><00:00:09.389><c> wonderful</c></c>
 */

const remote = require('electron').remote;
const fs = require('fs');

// remove tags from text
// all '<' and '>' and everything between
// tags in text must be valid
function removeTags(text) {
    let REGEX_TAGS = /(<([^>]+)>)/ig;
    return text.replace(REGEX_TAGS, "");
}

function getFirstLastTime(text) {
    let REGEX_TIMES = /\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+/mig;
    let re_times = text.match(REGEX_TIMES);
    if (re_times != null && re_times.length > 1) {
        return {
            'first': re_times[0],
            'last': re_times[re_times.length - 1]
        }
    }
    return null;
}

function loadCaptions(path) {
    const REGEX_TIME_START_END = /(\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+)\s+.*(\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+)/;

    let contents = fs.readFileSync(path, 'utf8');
    let contents_lines = contents.split('\n');

    let captions_json = [];

    for (let i = 0; i < contents_lines.length; i++) {
        let re_time = REGEX_TIME_START_END.exec(contents_lines[i]);
        let start_time_str, end_time_str;
        // console.log(i + ". " + contents_lines[i]);

        if (re_time != null) {
            let caption_json = {
                'start_time_display_str' : re_time[1],  // subtitle display time
                'end_time__display_str'  : re_time[2],
                'start_time_words_str'   : 0,
                'end_time_words_str'     : 0,
                'content'                : ""
            };
            let content = "";

            for (let j = i + 1; j < contents_lines.length; j++) {
                let re_time_other = REGEX_TIME_START_END.exec(contents_lines[j]); // tam kde je novy cas tak tam konci predosli

                if (re_time_other != null) {
                    break;
                }

                if (contents_lines[j] != null && contents_lines[j] != '' && contents_lines[j].length > 0) {
                    content += contents_lines[j];
                }
            }
            let start_end_time_words = getFirstLastTime(content);

            if (start_end_time_words != null) {
                caption_json.start_time_words_str = start_end_time_words.first;
                caption_json.end_time_words_str = start_end_time_words.last;

                caption_json.content = removeTags(content);

                captions_json.push(caption_json);
            }
        }
    }
    return captions_json;
}


let captions = loadCaptions("./data/President Trump Full Speech at the American Center for Mobility 3_15_17-CgmEosUZaWs.en.vtt");


fs.writeFileSync("./data/captions.txt", JSON.stringify(captions));






