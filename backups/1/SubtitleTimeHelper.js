const REGEX_SUB_TIME = /\s*(\d\d\:\d\d\:\d\d[.,]\d\d\d)\s*/;

module.exports.strToMS = function(str) { // string hh:mm:ss.msmsms to miliseconds
    let ms = 0;

    let ms_split = str.split('.');
    if (ms_split.length > 1) {
        ms += +ms_split[1];
    }
    else {
        ms_split = str.split(',');

        if (ms_split.length > 1) {
            ms += +ms_split[1];
        }
        else {
            // nieco je zle (milisekundy neoddeluje bodka ani ciarka) ?
        }
    }

    // zvysok je oddeleny dvojbodkami hh:mm:ss
    let rest_split = ms_split[0].split(':');
    ms += +rest_split[2] * 1000;
    ms += +rest_split[1] * 1000 * 60;
    ms += +rest_split[0] * 1000 * 60 * 60;
    return ms;
};

module.exports.strTimeIsValid = function(str) {
    re_time = REGEX_SUB_TIME.exec(str);

    if (re_time == null) {
        return false;
    }
    return true;
};

module.exports.MSToStr = function(ms, delimiter) { // miliseconds to 'hh:mm:ss.msmsms'
    let hours = parseInt(ms / (1000 * 60 * 60));
    let rest = parseInt(ms % (1000 * 60 * 60));
    let minutes = parseInt(rest / (1000 * 60));
    rest = rest % (1000 * 60);
    let seconds = parseInt(rest / 1000);
    rest = rest % 1000;
    return add_zeros(hours + '', 2) + ":" + add_zeros(minutes + '', 2) + ":" + add_zeros(seconds + '', 2) + delimiter + add_zeros(rest + '', 3);
};

// add zeros, e.g.: '1' => '01'
let add_zeros = function(nmbr, c) {
    if (nmbr.length < c) {
        to_add = '';
        for (let i = 0; i < c - nmbr.length; i++) {
            to_add += '0';
        }
        nmbr = to_add + nmbr;
    }
    return nmbr;
};

