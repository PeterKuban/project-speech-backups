// SRT
// aj multiline to 1 riadku

let fs = require('fs');

let subtitles_info = {
    path: '',
    extension: '',
    type: '',
    kind: '',
    lang: ''
}

subtitles_info.path = 'subs/Sheeran - Shape of you.srt'; // TODO: full path

const REGEX_FILE_EXTENSION = /\.(.*)$/;

const REGEX_TIME_START_END = /(\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+)\s+.*(\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+)/; // g = global search (viac ako 1 vyskyt)

fs.readFile(subtitles_info.path, 'utf8', function(err, data) {
    if (err) {
        return console.log(err);
    }

    let re_extension = REGEX_FILE_EXTENSION.exec(subtitles_info.path);
    if (re_extension != null) {
        subtitles_info.extension = re_extension[1];
    }
    else {
        subtitles_info.extension = 'unknown';
    }
    lines = data.split('\n');

    // zmenit vsetko na male pismena
    for (let i = 0; i < lines.length; i++) {
        lines[i] = lines[i].toLowerCase();
    }

    // function getSubtitlesInfo(file? raw_text?)
    subtitles_info.type = 'srt';
    subtitles_info.kind = 'unknown';
    subtitles_info.lang = 'en';

    console.log(subtitles_info);

    let wordized_srt_text = '';

    // SUBTITLES parsing (type 1 => whole sentences)
    for (let i = 0; i < lines.length; i++) {
        let re_time = REGEX_TIME_START_END.exec(lines[i]);
        let start_time_str, end_time_str;
        let text = '';

        if (re_time != null) {
            start_time_str = re_time[1];
            end_time_str = re_time[2];
            // console.log(start_time_str + ' => ' + end_time_str);

            for (let j = i + 1; j < lines.length; j++) {
                re_time = REGEX_TIME_START_END.exec(lines[j]);
                // console.log(j);
                if (re_time != null) { // nova sekcia
                    break;
                }

                if (lines[j] != null && lines[j] != '' && lines[j].length > 0) {
                    text += ' ' + lines[j]; // prida to sice medzeru aj na zaciatok ale tu potom odstranime
                }
            }
            let words = wordizeTextType2(strToMS(start_time_str), strToMS(end_time_str), text);

            for (let j = 0; j < words.length; j++) {
                wordized_srt_text += MSToStr(words[j].start_time, ',') + ' --> ' + MSToStr(words[j].end_time, ',');
                wordized_srt_text += '\n';
                wordized_srt_text += words[j].word;
                wordized_srt_text += '\n\n';
            }
        }

        // if (i > 10) {
        //     break;
        // }
    }
    // console.log(wordized_srt_text);

    fs.writeFile('output.srt', wordized_srt_text, 'utf8', function(err, data) {});

});


function strToMS(str) { // string hh:mm:ss.msmsms to miliseconds
    let ms = 0;

    let ms_split = str.split('.');
    if (ms_split.length > 1) {
        ms += +ms_split[1];
    }
    else {
        ms_split = str.split(',');
        if (ms_split.length > 1) {
            ms += +ms_split[1];
        }
        else {
            // nieco je zle (milisekundy neoddeluje bodka ani ciarka) ?
        }
    }

    // zvysok je oddeleny dvojbodkami hh:mm:ss
    let rest_split = ms_split[0].split(':');
    ms += +rest_split[2] * 1000;
    ms += +rest_split[1] * 1000 * 60;
    ms += +rest_split[0] * 1000 * 60 * 60;
    return ms;
}

function MSToStr(ms, delimiter) { // miliseconds to 'hh:mm:ss.msmsms'
    let hours = parseInt(ms / (1000 * 60 * 60));
    let rest = parseInt(ms % (1000 * 60 * 60));
    let minutes = parseInt(rest / (1000 * 60));
    rest = rest % (1000 * 60);
    let seconds = parseInt(rest / 1000);
    rest = rest % 1000;
    return add_zeros(hours + '', 2) + ":" + add_zeros(minutes + '', 2) + ":" + add_zeros(seconds + '', 2) + delimiter + add_zeros(rest + '', 3);
}

// add zeros, e.g.: '1' => '01'
function add_zeros(nmbr, c) {

    if (nmbr.length < c) {
        to_add = '';
        for (i = 0; i < c - nmbr.length; i++) {
            to_add += '0';
        }
        nmbr = to_add + nmbr;
    }
    return nmbr;
}

// from text (more word) and start & end times creates estimated times for each word
function wordizeTextType2(_start_time, _end_time, text) {
    let ms_for_letter = 150; // how many miliseconds are required to say a letter (TODO)
    let ms_for_space = 20;

    words = [];
    let parts = text.split(' ');

    if (parts.length < 1) {
        return [];
    }

    let start_time = _start_time;
    let end_time   = _end_time;

    for (let i = 0; i < parts.length; i++) {
        parts[i] = parts[i].trim();

        if (parts[i].length < 1) {
            continue;
        }
        // console.log(parts[i]);
        let word = parts[i];
        let word_start_time = start_time;
        let word_end_time   = word_start_time + word.length * ms_for_letter;
        let word_data = {
            word: word,
            start_time: word_start_time,
            end_time: word_end_time
        };
        words.push(word_data);

        start_time = word_start_time + ms_for_space;

        if (start_time > end_time) {
            console.error('start_time > end_time!!!');
        }

    }

    return words;
}