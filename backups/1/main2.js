// VTT TYPE 2
// returns WORDIZED subtitles
// VTT su tie co maju este aj v texte casy

let fs = require('fs');

let subtitles_info = {
    path: '',
    extension: '',
    type: '',
    kind: '',
    lang: ''
}

subtitles_info.path = 'subs/Sheeran - Shape of you.srt'; // TODO: full path

const REGEX_FILE_EXTENSION = /\.(.*)$/;

const REGEX_VTT_KIND = /Kind:(.*)/i; // i = case insensitive
const REGEX_VTT_LANGUAGE = /Language:(.*)/i;

const REGEX_VTT_TIME_START_END = /(\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+)\s+.*(\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+)/; // g = global search (viac ako 1 vyskyt)

const REGEX_STRIP_TEXT = /[^\w\s]/gi; // only alphanumeric (to remove .,:...), keep spaces


const REGEX_VTT_TIME_INSIDE = /<(\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+)>/;


fs.readFile(subtitles_info.path, 'utf8', function(err, data) {
    if (err) {
        return console.log(err);
    }

    let re_extension = REGEX_FILE_EXTENSION.exec(subtitles_info.path);
    if (re_extension != null) {
        subtitles_info.extension = re_extension[1];
    }
    else {
        subtitles_info.extension = 'unknown';
    }
    lines = data.split('\n');

    // zmenit vsetko na male pismena
    for (let i = 0; i < lines.length; i++) {
        lines[i] = lines[i].toLowerCase();
    }

    // function getSubtitlesInfo(file? raw_text?)

    // vyber zakladnych informacii o titulkoch (playi pre .vtt)
    if (lines[0] == 'webvtt') { // co ine tam moze byt?
        subtitles_info.type = 'webvtt';
    }
    else {
        subtitles_info.type = 'unknown';
    }

    let re_kind = REGEX_VTT_KIND.exec(lines[1]);
    if (re_kind != null) {
        subtitles_info.kind = re_kind[1].trim();
    }
    else {
        subtitles_info.kind = 'unknown';
    }

    let re_language = REGEX_VTT_LANGUAGE.exec(lines[2]);
    if (re_language != null) {
        subtitles_info.lang = re_language[1].trim();
    }
    else {
        subtitles_info.lang = 'unknown';
    }

    console.log(subtitles_info);

    let wordized_srt_text = '';

    // SUBTITLES parsing (type 1 => whole sentences)
    for (let i = 0; i < lines.length; i++) {
        let re_time = REGEX_VTT_TIME_START_END.exec(lines[i]);
        let start_time_str, end_time_str;
        let text = '';

        if (re_time != null) {
            start_time_str = re_time[1];
            end_time_str = re_time[2];
            // console.log(start_time_str + ' => ' + end_time_str);
            for (let j = i + 1; j < lines.length; j++) {
                re_time = REGEX_VTT_TIME_START_END.exec(lines[j]);
                if (re_time != null) { // nova sekcia
                    break;
                }
                text += ' ' + lines[j]; // prida to sice medzeru aj na zaciatok ale tu potom odstranime
            }
            // console.log(start_time_str + " => " + strToMS(start_time_str) + " => " + MSToStr(strToMS(start_time_str), '.'));
            // console.log(end_time_str + " => " + strToMS(end_time_str) + " => " + MSToStr(strToMS(end_time_str), '.'));
            // console.log(text);

            let words = wordizeTextType2(strToMS(start_time_str), strToMS(end_time_str), text);

            for (let j = 0; j < words.length; j++) {
                wordized_srt_text += MSToStr(words[j].start_time, ',') + ' --> ' + MSToStr(words[j].end_time, ',');
                wordized_srt_text += '\n';
                wordized_srt_text += words[j].word;
                wordized_srt_text += '\n\n';
            }
            // console.log(words);
        }

        // if (i > 10) {
        //     break;
        // }
    }
    // console.log(wordized_srt_text);

    fs.writeFile('output.srt', wordized_srt_text, 'utf8', function(err, data) {});

});


function strToMS(str) { // string hh:mm:ss.msmsms to miliseconds
    let ms = 0;

    let ms_split = str.split('.');
    if (ms_split.length > 1) {
        ms += +ms_split[1];
    }
    else {
        ms_split = str.split(',');
        if (ms_split.length > 1) {
            ms += +ms_split[1];
        }
        else {
            // nieco je zle (milisekundy neoddeluje bodka ani ciarka) ?
        }
    }

    // zvysok je oddeleny dvojbodkami hh:mm:ss
    let rest_split = ms_split[0].split(':');
    ms += +rest_split[2] * 1000;
    ms += +rest_split[1] * 1000 * 60;
    ms += +rest_split[0] * 1000 * 60 * 60;
    return ms;
}

function MSToStr(ms, delimiter) { // miliseconds to 'hh:mm:ss.msmsms'
    let hours = parseInt(ms / (1000 * 60 * 60));
    let rest = parseInt(ms % (1000 * 60 * 60));
    let minutes = parseInt(rest / (1000 * 60));
    rest = rest % (1000 * 60);
    let seconds = parseInt(rest / 1000);
    rest = rest % 1000;
    return add_zeros(hours + '', 2) + ":" + add_zeros(minutes + '', 2) + ":" + add_zeros(seconds + '', 2) + delimiter + add_zeros(rest + '', 3);
}

// add zeros, e.g.: '1' => '01'
function add_zeros(nmbr, c) {

    if (nmbr.length < c) {
        to_add = '';
        for (i = 0; i < c - nmbr.length; i++) {
            to_add += '0';
        }
        nmbr = to_add + nmbr;
    }
    return nmbr;
}

// from text (more word) and start & end times creates estimated times for each word
function wordizeTextType2(start_time, end_time, text) {
    let ms_for_letter = 150; // how many miliseconds are required to say a letter (TODO)
    let ms_for_word_space = 20;

    words = [];

    let parts = text.split('</c>');
    // console.log(text);
    // console.log(parts);

    if (parts.length < 2) {
        return [];
    }

    for (let i = 0; i < parts.length; i++) {

        parts[i] = parts[i].trim();

        if (parts[i].length < 2) {
            continue;
        }

        if (i == 0) { // ' as<00:00:00.659><c> one',
            let splitted_part = parts[i].split('<c>');

            if (splitted_part.length < 2) {
                continue;
            }
            // console.log(splitted_part);
            // console.log(splitted_part[0].split('<')[0]);
            let word_1 = splitted_part[0].split('<')[0].trim();
            let word_1_start_time = start_time;
            let word_1_end_time = word_1_start_time + word_1.length * ms_for_letter;

            let word_data = {
                word: word_1,
                start_time: word_1_start_time,
                end_time: word_1_end_time
            };
            words.push(word_data);

            let word_2 = splitted_part[1].trim();
            let re_start_time = REGEX_VTT_TIME_INSIDE.exec(parts[i]);
            let word_2_start_time = strToMS(re_start_time[1]);
            let word_2_end_time = word_2_start_time + word_2.length * ms_for_letter;

            word_data = {
                word: word_2,
                start_time: word_2_start_time,
                end_time: word_2_end_time
            };
            words.push(word_data);
        }
        else { // '<00:00:01.050><c> small', or '<c.colorcccccc><00:00:02.040><c> man',
            let word = parts[i].split('<c>')[1].trim();
            let re_start_time = REGEX_VTT_TIME_INSIDE.exec(parts[i].split('<c>')[0]);
            let word_start_time = strToMS(re_start_time[1]);
            let word_end_time = word_start_time + word.length * ms_for_letter;
            let word_data = {
                word: word,
                start_time: word_start_time,
                end_time: word_end_time
            };
            words.push(word_data);
        }

    }

    /*
    if (time > end_time) {
        console.error('TIME > END_TIME !!!');
    }
    */
    return words;
}