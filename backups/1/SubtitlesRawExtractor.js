let subtitleTimeHelper = require('./SubtitleTimeHelper.js');

module.exports.extractRawSubsFromFile = function(filefullpath, callback) {
    let fs = require('fs');

    const REGEX_LINE_WITH_SINGLE_NUMBER = /^\s*\d+\s*$/;

    const REGEX_SUBS_TIME_START_END = /(\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+)\s+.*(\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+)/; // g = global search (viac ako 1 vyskyt)

    let subsJSON = [];
    fs.readFile(filefullpath, 'utf8', function(err, data) {
        if (err) {
            console.error("ERROR WITH READING FILE: " + filefullpath);
        }
        lines = data.split('\n');

        // zmenit vsetko na male pismena
        for (let i = 0; i < lines.length; i++) {
            lines[i] = lines[i].toLowerCase();
        }

        let re_time, start_time_str, end_time_str, text;
        for (let i = 0; i < lines.length; i++) {

            if (lines[i].match(REGEX_LINE_WITH_SINGLE_NUMBER)) { // ignore lines with only one number
                continue;
            }
            re_time = REGEX_SUBS_TIME_START_END.exec(lines[i]);
            start_time_str = '';
            end_time_str = '';
            text = '';

            if (re_time != null) {
                start_time_str = re_time[1];
                end_time_str = re_time[2];

                for (let j = i + 1; j < lines.length; j++) {
                    re_time = REGEX_SUBS_TIME_START_END.exec(lines[j]);
                    if (re_time != null) { // nova sekcia
                        break;
                    }
                    text += ' ' + lines[j]; // prida to sice medzeru aj na zaciatok ale tu potom odstranime
                }
                // console.log(start_time_str + " => " + strToMS(start_time_str) + " => " + MSToStr(strToMS(start_time_str), '.'));
                // console.log(end_time_str + " => " + strToMS(end_time_str) + " => " + MSToStr(strToMS(end_time_str), '.'));
                // console.log(text);

                subsJSON.push({
                    'time_start': subtitleTimeHelper.strToMS(start_time_str),
                    'time_end'  : subtitleTimeHelper.strToMS(end_time_str),
                    'text'      : text.trim()
                });

            }
        }

        callback(subsJSON);

    });

}