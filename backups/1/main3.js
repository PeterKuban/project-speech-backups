// REMOVE LINES WITH JUST NUMBER (some subtitles are numbered ?)

let fs = require('fs');

subtitles_info = {};
subtitles_info.path = '/Users/larry/Desktop/project_speech/music/Ed-Sheeran-Shape-Of-You-(Official-Video).srt'; // TODO: full path

const REGEX_LINE_WITH_SINGLE_NUMBER = /^\s*\d+\s*$/;

fs.readFile(subtitles_info.path, 'utf8', function(err, data) {

    if (err) {
        console.error("ERROR WITH READ FILE");
    }
    lines = data.split('\n');

    // zmenit vsetko na male pismena
    for (let i = 0; i < lines.length; i++) {
        lines[i] = lines[i].toLowerCase();
    }

    let new_output = '';
    for (let i = 0; i < lines.length; i++) {

        if (lines[i].match(REGEX_LINE_WITH_SINGLE_NUMBER)) { // ignore lines with only one number
            continue;
        }
        new_output += lines[i] + '\n';
    }

    fs.writeFile('/Users/larry/Desktop/project_speech/music/Ed-Sheeran-Shape-Of-You-(Official-Video)-new.srt', new_output, 'utf8', function(err, data) {});
});