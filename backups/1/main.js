// VTT TYPE 1
// returns WORDIZED subtitles

let fs = require('fs');

let subtitles_info = {
    path: '',
    extension: '',
    type: '',
    kind: '',
    lang: ''
}

subtitles_info.path = 'subs/subs_type_1.vtt'; // TODO: full path

// TODO: .srt ?

const REGEX_FILE_EXTENSION = /\.(.*)$/;

const REGEX_SUBS_KIND = /Kind:(.*)/i; // i = case insensitive
const REGEX_SUBS_LANGUAGE = /Language:(.*)/i;

const REGEX_SUBS_TIME_START_END = /(\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+)\s+.*(\d+\d+\:\d+\d+\:\d+\d+[.,]\d+\d+\d+)/; // g = global search (viac ako 1 vyskyt)

const REGEX_STRIP_TEXT = /[^\w\s]/gi; // only alphanumeric (to remove .,:...), keep spaces

fs.readFile(subtitles_info.path, 'utf8', function(err, data) {
    if (err) {
        return console.log(err);
    }

    let re_extension = REGEX_FILE_EXTENSION.exec(subtitles_info.path);
    if (re_extension != null) {
        subtitles_info.extension = re_extension[1];
    }
    else {
        subtitles_info.extension = 'unknown';
    }


    lines = data.split('\n');

    // zmenit vsetko na male pismena
    for (let i = 0; i < lines.length; i++) {
        lines[i] = lines[i].toLowerCase();
    }

    // function getSubtitlesInfo(file? raw_text?)

    // vyber zakladnych informacii o titulkoch (playi pre .vtt)
    if (lines[0] == 'webvtt') { // co ine tam moze byt?
        subtitles_info.type = 'webvtt';
    }
    else {
        subtitles_info.type = 'unknown';
    }

    let re_kind = REGEX_SUBS_KIND.exec(lines[1]);
    if (re_kind != null) {
        subtitles_info.kind = re_kind[1].trim();
    }
    else {
        subtitles_info.kind = 'unknown';
    }

    let re_language = REGEX_SUBS_LANGUAGE.exec(lines[2]);
    if (re_language != null) {
        subtitles_info.lang = re_language[1].trim();
    }
    else {
        subtitles_info.lang = 'unknown';
    }

    // console.log(subtitles_info);

    let wordized_srt_text = '';

    // SUBTITLES parsing (type 1 => whole sentences)
    for (let i = 0; i < lines.length; i++) {
        let re_time = REGEX_SUBS_TIME_START_END.exec(lines[i]);
        let start_time_str, end_time_str;
        let text = '';

        if (re_time != null) {
            start_time_str = re_time[1];
            end_time_str = re_time[2];

            for (let j = i + 1; j < lines.length; j++) {
                re_time = REGEX_SUBS_TIME_START_END.exec(lines[j]);
                if (re_time != null) { // nova sekcia
                    break;
                }
                text += ' ' + lines[j]; // prida to sice medzeru aj na zaciatok ale tu potom odstranime
            }
            // console.log(start_time_str + " => " + strToMS(start_time_str) + " => " + MSToStr(strToMS(start_time_str), '.'));
            // console.log(end_time_str + " => " + strToMS(end_time_str) + " => " + MSToStr(strToMS(end_time_str), '.'));
            // console.log(text);

            let words = wordizeText(strToMS(start_time_str), strToMS(end_time_str), text);

            for (let j = 0; j < words.length; j++) {
                wordized_srt_text += MSToStr(words[j].start_time, ',') + ' --> ' + MSToStr(words[j].end_time, ',');
                wordized_srt_text += '\n';
                wordized_srt_text += words[j].word;
                wordized_srt_text += '\n\n';
            }
        }

        // if (i > 3) {
        //     break;
        // }
    }
    // console.log(wordized_srt_text);

    fs.writeFile('output.srt', wordized_srt_text, 'utf8', function(err, data) {});

});


function strToMS(str) { // string hh:mm:ss.msmsms to miliseconds
    let ms = 0;

    let ms_split = str.split('.');
    if (ms_split.length > 1) {
        ms += +ms_split[1];
    }
    else {
        ms_split = str.split(',');
        if (ms_split.length > 1) {
            ms += +ms_split[1];
        }
        else {
            // nieco je zle (milisekundy neoddeluje bodka ani ciarka) ?
        }
    }

    // zvysok je oddeleny dvojbodkami hh:mm:ss
    let rest_split = ms_split[0].split(':');
    ms += +rest_split[2] * 1000;
    ms += +rest_split[1] * 1000 * 60;
    ms += +rest_split[0] * 1000 * 60 * 60;
    return ms;
}

function MSToStr(ms, delimiter) { // miliseconds to 'hh:mm:ss.msmsms'
    let hours = parseInt(ms / (1000 * 60 * 60));
    let rest = parseInt(ms % (1000 * 60 * 60));
    let minutes = parseInt(rest / (1000 * 60));
    rest = rest % (1000 * 60);
    let seconds = parseInt(rest / 1000);
    rest = rest % 1000;
    return add_zeros(hours + '', 2) + ":" + add_zeros(minutes + '', 2) + ":" + add_zeros(seconds + '', 2) + delimiter + add_zeros(rest + '', 3);
}

// add zeros, e.g.: '1' => '01'
function add_zeros(nmbr, c) {

    if (nmbr.length < c) {
        to_add = '';
        for (i = 0; i < c - nmbr.length; i++) {
            to_add += '0';
        }
        nmbr = to_add + nmbr;
    }
    return nmbr;
}

// from text (more word) and start & end times creates estimated times for each word
function wordizeText(start_time, end_time, text) {
    let ms_for_letter = 50; // how many miliseconds are required to say a letter (TODO)
    let ms_for_word_space = 20;

    words = [];

    let text_alphanumeric = text.replace(REGEX_STRIP_TEXT, ''); // strip .,:;...
    let _words = text_alphanumeric.split(' '); // split to words
    let word;
    let time = start_time;
    for (let i = 0; i < _words.length; i++) {
        word = _words[i].trim();

        if (word.length > 0) {
            let word_data = {};
            word_data.word = word;
            word_data.start_time = time;
            time += word.length * ms_for_letter;
            time += ms_for_word_space;
            word_data.end_time = time;
            words.push(word_data);
        }
    }
    if (time > end_time) {
        console.error('TIME > END_TIME !!!');
    }
    return words;
}