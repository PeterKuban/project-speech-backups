let subtitleTimeHelper = require('/Users/larry/Dev/node/learn/1/SubtitleTimeHelper.js');

let subtitle_id = 0;
module.exports = function() {
    let that = this;
    this.element = document.createElement('div');
    this.element.className += 'subtitles-list';

    this.buttonOffsetLeft1000Element = document.createElement('button');
    this.buttonOffsetLeft1000Element.innerHTML = 'Offset << 1s';
    this.element.appendChild(this.buttonOffsetLeft1000Element);

    this.buttonOffsetLeft500Element = document.createElement('button');
    this.buttonOffsetLeft500Element.innerHTML = 'Offset << 500ms';
    this.element.appendChild(this.buttonOffsetLeft500Element);

    this.buttonOffsetLeft100Element = document.createElement('button');
    this.buttonOffsetLeft100Element.innerHTML = 'Offset << 100ms';
    this.element.appendChild(this.buttonOffsetLeft100Element);

    this.buttonOffsetRight100Element = document.createElement('button');
    this.buttonOffsetRight100Element.innerHTML = 'Offset >> 100ms';
    this.element.appendChild(this.buttonOffsetRight100Element);

    this.buttonOffsetRight500Element = document.createElement('button');
    this.buttonOffsetRight500Element.innerHTML = 'Offset >> 500ms';
    this.element.appendChild(this.buttonOffsetRight500Element);

    this.buttonOffsetRight1000Element = document.createElement('button');
    this.buttonOffsetRight1000Element.innerHTML = 'Offset >> 1s';
    this.element.appendChild(this.buttonOffsetRight1000Element);

    this.element.appendChild(document.createElement('br'));

    this.buttonSaveSRTElement = document.createElement('button');
    this.buttonSaveSRTElement.innerHTML = 'Save SRT';
    this.element.appendChild(this.buttonSaveSRTElement);

    this.buttonSaveJSONElement = document.createElement('button');
    this.buttonSaveJSONElement.innerHTML = 'Save JSON';
    this.element.appendChild(this.buttonSaveJSONElement);

    this.locked_hidden = false;
    this.buttonHideLockedElement = document.createElement('button');
    this.buttonHideLockedElement.innerHTML = 'Hide Locked';
    this.element.appendChild(this.buttonHideLockedElement);

    this.buttonShowLockedElement = document.createElement('button');
    this.buttonShowLockedElement.innerHTML = 'Show Locked';
    this.element.appendChild(this.buttonShowLockedElement);

    this.tableElement = document.createElement('table');
    this.tableElement.className += 'subtitles-list-table';
    this.element.appendChild(this.tableElement);

    this.subtitles = [];

    this.addSubtitle = function(jsonSubtitle) {
        let newSubtitle = new Subtitle(jsonSubtitle);
        this.subtitles.push(newSubtitle);

        this.tableElement.appendChild(newSubtitle.element);
    };

    this.getSubtitle = function(index) {
        return this.subtitles[index];
    };

    this.updateSubtitle = function(index, jsonSubtitle) {
        this.subtitles[index].setTimeStart(jsonSubtitle['time_start']);
        this.subtitles[index].setTimeEnd(jsonSubtitle['time_end']);
        this.subtitles[index].setText(jsonSubtitle['text']);
    };

    this.mergeSubtitles = function(first_subtitle, second_subtitle) {
        let new_start = first_subtitle.time_start;
        let new_end   = second_subtitle.time_end;
        let new_text  = first_subtitle.text + ' ' + second_subtitle.text;

        let jsonMerged = {
            'time_start': new_start,
            'time_end': new_end,
            'text': new_text
        };

        this.updateSubtitle(first_subtitle.id, jsonMerged);
        this.updateSubtitle(second_subtitle.id, {'time_start': 0.0, 'time_end': 0.0, 'text': '-'});

        this.subtitles[second_subtitle.id].buttonsElement2.innerHTML = ''; // merge top
        this.subtitles[second_subtitle.id].buttonsElement3.innerHTML = ''; // merge bottom
        this.subtitles[first_subtitle.id].buttonsElement3.innerHTML = '';
        this.subtitles[second_subtitle.id + 1].buttonsElement2.innerHTML = '';
    };

    this.offsetAllSubtitles = function(offset_value) {
        for (let i = 0; i < that.subtitles.length; i++) {
            that.subtitles[i].setTimeStart(that.subtitles[i].time_start + offset_value);
            that.subtitles[i].setTimeEnd(that.subtitles[i].time_end + offset_value);
        }
    };

    this.saveSRT = function(output_filename){
        let srt_text = '';
        for (let j = 0; j < this.subtitles.length; j++) {
            srt_text += subtitleTimeHelper.MSToStr(this.subtitles[j].time_start, ',') + ' --> ' + subtitleTimeHelper.MSToStr(this.subtitles[j].time_end, ',');
            srt_text += '\n';
            srt_text += (this.subtitles[j].text).trim();
            srt_text += '\n\n';
        }
        let fs = require('fs');
        fs.writeFile(output_filename, srt_text, 'utf8', function(err, data) {});
    };

    this.saveJSON = function(output_filename) {
        let json_object = [];
        for (let j = 0; j < this.subtitles.length; j++) {

            if (this.subtitles[j].time_start != 0.0 && this.subtitles[j].time_end != 0.0 && this.subtitles[j].text.trim() != '-') {
                json_object.push({
                    time_start: this.subtitles[j].time_start,
                    time_end: this.subtitles[j].time_end,
                    text: this.subtitles[j].text.trim(),
                    locked: this.subtitles[j].locked
                });
            }
        }
        let fs = require('fs');
        fs.writeFile(output_filename, JSON.stringify(json_object), 'utf8', function(err, data) {});
    };
    /*
    this.toggleLocked = function() {
        this.locked_hidden = !this.locked_hidden;

        for (let i = 0; i < that.subtitles.length; i++) {

            if (this.locked_hidden && this.subtitles[i].locked) {
                that.subtitles[i].element.style.display = 'none';
            }
            else {
                that.subtitles[i].element.style.display = 'table-row';
            }
        }
    };
    */
    this.hideLocked = function() {
        for (let i = 0; i < that.subtitles.length; i++) {

            if (this.subtitles[i].locked) {
                that.subtitles[i].element.style.display = 'none';
            }
        }
    };

    this.showLocked = function() {
        for (let i = 0; i < that.subtitles.length; i++) {

            if (this.subtitles[i].locked) {
                that.subtitles[i].element.style.display = 'table-row';
            }
        }
    };

    this.buttonOffsetLeft1000Element.onmousedown = function(me) { that.offsetAllSubtitles(-1000); };
    this.buttonOffsetLeft500Element.onmousedown = function(me) { that.offsetAllSubtitles(-500); };
    this.buttonOffsetLeft100Element.onmousedown = function(me) { that.offsetAllSubtitles(-100); };
    this.buttonOffsetRight1000Element.onmousedown = function(me) { that.offsetAllSubtitles(1000); };
    this.buttonOffsetRight500Element.onmousedown = function(me) { that.offsetAllSubtitles(500); };
    this.buttonOffsetRight100Element.onmousedown = function(me) { that.offsetAllSubtitles(100); };
    this.buttonSaveSRTElement.onmousedown = function(me) { that.saveSRT('output.srt'); };
    this.buttonSaveJSONElement.onmousedown = function(me) { that.saveJSON('output.json') };
    this.buttonHideLockedElement.onmousedown = function(me) { that.hideLocked() };
    this.buttonShowLockedElement.onmousedown = function(me) { that.showLocked() };
};

function Subtitle(jsonSubtitle) {
    let that = this;

    this.id = subtitle_id++;

    this.time_start = jsonSubtitle['time_start'];
    this.time_end   = jsonSubtitle['time_end'];
    this.text       = jsonSubtitle['text'];
    this.locked     = jsonSubtitle['locked'] | false;

    this.element = document.createElement('tr');
    this.element.className += 'subtitle';
    this.element.innerHTML = '<td></td><td></td><td></td><td></td><td></td><td></td>';

    let allTds = this.element.getElementsByTagName('td');
    this.timeStartElement = allTds[0];
    this.timeEndElement   = allTds[1];
    this.textElement      = allTds[2];
    this.buttonsElement   = allTds[3];
    this.buttonsElement2  = allTds[4];
    this.buttonsElement3  = allTds[5];

    this.lockButtonElement = document.createElement('button');
    this.lockButtonElement.innerHTML = this.locked ? 'LOCKED' : 'UNLOCKED';
    this.buttonsElement.appendChild(this.lockButtonElement);
    this.lockButtonElement.onmousedown = function() {
        that.toggleLock();
    };

    // dal som to sem lebo z jsonu sa berie aj locked parameter (a ked je locked tak nefunguje set...)
    this.timeStartElement.innerHTML = subtitleTimeHelper.MSToStr(this.time_start, ',');
    this.timeEndElement.innerHTML = subtitleTimeHelper.MSToStr(this.time_end, ',');
    this.textElement.innerHTML = this.text;


    this.setTimeStart = function(time_start) {
        if (!this.locked) {
            this.time_start = time_start;
            this.timeStartElement.innerHTML = subtitleTimeHelper.MSToStr(this.time_start, ',');
        }
    };

    this.setTimeEnd = function(time_end) {
        if (!this.locked) {
            this.time_end = time_end;
            this.timeEndElement.innerHTML = subtitleTimeHelper.MSToStr(this.time_end, ',');
        }
    };

    this.setText = function(text) {
        if (!this.locked) {
            this.text = text;
            this.textElement.innerHTML = text;
        }
    };

    this.getJson = function() {
      return {
          'time_start': this.time_start,
          'time_end': this.time_end,
          'text': this.text
      };
    };

    this.toggleLock = function() {
        this.locked = !this.locked;
        this.lockButtonElement.innerHTML = this.locked ? 'LOCKED' : 'UNLOCKED';
    };

    this.lock = function() {
        this.locked = true;
        this.lockButtonElement.innerHTML = this.locked ? 'LOCKED' : 'UNLOCKED';
    };

    this.unlock = function() {
        this.locked = false;
        this.lockButtonElement.innerHTML = this.locked ? 'LOCKED' : 'UNLOCKED';
    };

    this.focus = function() {
        this.element.style.color = 'yellow';
    };

    this.unfocus = function() {
        this.element.style.color = 'white';
    };

    this.setTimeStart(this.time_start);
    this.setTimeEnd(this.time_end);
    this.setText(this.text);



}
