let subtitleTimeHelper = require('/Users/larry/Dev/node/learn/1/SubtitleTimeHelper.js');
let id_counter = 0;

function ObjectUnderCursor() {
    this.is_timeline = false;
    this.is_slider = false;
    this.set = function(mouseEvent) {
        this.is_timeline = false;
        this.is_slider = false;
        this.is_slider = mouseEvent.srcElement.className.indexOf('tlsc-slider') !== -1;
        this.is_timeline = (mouseEvent.srcElement.className.indexOf('tlsc') !== -1) && !this.is_slider;
        return mouseEvent.srcElement;
    }
    this.unset = function() {
        this.is_timeline = false;
        this.is_slider = false;
    }
}

function TimelineSubtitleControl(subtitle_data) { // tl = timeline, sc = subtitle control
    let that = this;
    this.id = id_counter++;
    this.data = subtitle_data;

    this.start_time_ms = subtitleTimeHelper.strToMS(this.data.start_time_words_str);
    this.end_time_ms = subtitleTimeHelper.strToMS(this.data.end_time_words_str);

    this.timeline = null;

    // this.resolution = 20.0; // 10px per second ?

    this.element = document.createElement('div');
    this.element.className += 'tlsc';

    // selected slider
    // onmousedown - podla toho kde a aky slider je selecnuty... atd.
    this.objectUnderMouse = new ObjectUnderCursor();
    this.objectUnderMouseDown = new ObjectUnderCursor();
    this.objectUnderMouseUp = new ObjectUnderCursor();

    this.setTimeline = function(tl) {
        this.timeline = tl;
    };

    this.setPositionAndSize = function(x, y, width, height) {
        this.element.style.left   = x + 'px';
        this.element.style.top    = y + 'px';
        this.element.style.width  = width + 'px';
        this.element.style.height = height + 'px';
    };

    this.getElement = function() {
        return this.element;
    };

    this.getId = function() {
        return this.id;
    };

    this.getStartTimeMS = function() {
        return this.start_time_ms;
    };

    this.getEndTimeMS = function() {
        return this.end_time_ms;
    };

    this.slider = new Slider(this);
    this.element.appendChild(this.slider.getElement());


    this.element.onmousedown = function(me) {
        that.objectUnderMouse.set(me);
        that.objectUnderMouseDown.set(me);
        console.log('is_slider = ' + that.objectUnderCursor.is_slider + ' | is_timeline = ' + that.objectUnderCursor.is_timeline);
    };

    this.element.onmousemove = function(me) {
        that.objectUnderMouse.set(me);
        console.log('is_slider = ' + that.objectUnderCursor.is_slider + ' | is_timeline = ' + that.objectUnderCursor.is_timeline);
    };

    this.element.onmouseup = function(me) {
        that.objectUnderMouse.set(me);
        that.objectUnderMouseUp.set(me);
        console.log('is_slider = ' + that.objectUnderCursor.is_slider + ' | is_timeline = ' + that.objectUnderCursor.is_timeline);
    };
}

function Slider(_subtitleControl) {
    let that = this;
    this.subtitleControl = _subtitleControl;

    this.element = document.createElement('div');
    this.element.className += 'tlsc-slider';

    this.x = 0.0;
    this.is_mouse_down = false;
    this.mouse_down_on = 0.0;
    this.mouse_up_on = 0.0;
    this.mouse_last_position = 0.0;
    this.mouse_current_position = 0.0;
    this.mouse_delta = 0.0;

    this.getElement = function() {
        return this.element;
    };

    this.element.onmousedown = function(me) {
        // console.log(me);
        that.is_mouse_down = true;
        that.mouse_down_on = me.clientX;
        that.mouse_current_position = me.clientX;
        that.mouse_last_position = me.clientX;
    };

    this.element.onmousemove = function(me) {
        that.mouse_current_position = me.clientX;
        that.mouse_delta = that.mouse_current_position - that.mouse_last_position;

        if (that.is_mouse_down) {
            that.x += that.mouse_delta;
            that.element.style.left = that.x + 'px';
        }
        that.mouse_last_position = me.clientX;
    };

    this.element.onmouseup = function(me) {
        that.is_mouse_down = false;
        that.mouse_up_on = that.clientX;
    };
}

module.exports = TimelineSubtitleControl;