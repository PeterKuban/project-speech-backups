let timelines_count = 0;

function Timeline(parentWindow) {
    this.parentWindow = parentWindow;

    this.line_counter = 0; // potrebujeme vediet na ktorom riadku bude nasledujuci subslider ...
    this.line_height     = 14.0;
    this.line_width_resolution = 20.0;

    this.element = document.createElement('div');
    this.element.id = 'timeline-' + (timelines_count++);
    this.element.style.position = 'relative';
    this.parentWindow.appendChild(this.element);

    this.subtitleControls = {};

    this.addSubtitleControl = function(subtitle_control) {
        subtitle_control.setTimeline(this);

        let x = (subtitle_control.getStartTimeMS() / 1000) * this.line_width_resolution;
        let y = this.line_height * this.line_counter;
        let width  = (subtitle_control.getEndTimeMS() / 1000) * this.line_width_resolution;
        let height = this.line_height;

        subtitle_control.setPositionAndSize(x, y, width, height);

        this.subtitleControls[subtitle_control.getId()] = {
            'subtitle_control'     : subtitle_control,
            'timeline_data' : {
                'line': this.getNextLine()
            }
        };
        this.element.appendChild(subtitle_control.getElement());
    };

    this.getElement = function() {
        return this.element;
    };

    this.getNextLine = function() {
        return this.line_counter++;
    }



}


module.exports = Timeline;