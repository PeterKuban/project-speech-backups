let fs = require('fs');

let Timeline = require('./timeline.js');
let SubtitleControl = require('./timeline_subtitle_control.js');

let timeline_0 = new Timeline(document.querySelector('#content'));


let data = fs.readFileSync("./data/captions.txt", "utf8");
let data_json = JSON.parse(data);


// console.log(data_json[0]);
let subtitle_control_0 = new SubtitleControl(data_json[0]);
timeline_0.addSubtitleControl(subtitle_control_0);

//console.log(data_json[1]);
let subtitle_control_1 = new SubtitleControl(data_json[1]);
timeline_0.addSubtitleControl(subtitle_control_1);
